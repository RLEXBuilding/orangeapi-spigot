package net.rlexbuilding.spigot.OrangeAPI;

import java.io.File;

import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.PluginDescriptionFile;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.plugin.java.JavaPluginLoader;

public abstract class OrangePlugin extends JavaPlugin{


	/**
	 * 
	 * @return Pluginname(String)
	 * 
	 * <br> Getter/Setter PluginName <br> this created automaticly from OrangeAPI
	 * @param "Your PluginName"
	 */
	public abstract String getPluginName();
	

	/**
	 * 
	 * @return Name from the author(String)

	 * <br> Getter/Setter Authors Name <br> this created automaticly from OrangeAPI
	 * @param "Your Name"
	 */
	public abstract String getPluginAuthor();
	
	/**
	 *  The Method is the start method of the plugin
	 *  <b>Example:</b>
	 *  <blockquote><pre><code>
	 *  public void onEnable(){
	 *  System.out.println("Plugin started!");
	 *  }
	 *  </code></pre></blockquote>
	 */
	@Override
	public void onEnable(){
		System.out.println("OrangeAPI-Plugin enabled: " + getPluginName());
	}
	
	@Override
	public void onLoad(){
		
		System.out.println("OrangeAPI-Plugin loaded: " + getPluginName());
	}

	@Override
	public void onDisable(){
		System.out.println("OrangeAPI-Plugin disabled: " + getPluginName());
	}
	
}
