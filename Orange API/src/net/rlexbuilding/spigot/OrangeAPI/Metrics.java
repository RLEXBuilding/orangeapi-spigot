/**
 * 
 */
package net.rlexbuilding.spigot.OrangeAPI;

import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.Socket;


/**
 * @author RLEXBuilding
 *
 */
public class Metrics {

	
	
	private static final String URI = "https://rlex.lima-city.de/user/rlexbuilding/coding/metrics/?plugin=OrangeAPI&&type=Spigot";

	@MetricsAnnonation(URI=URI)
	public Metrics(){
		
	Socket socket = new Socket();
	try {
		socket.bind(new InetSocketAddress(InetAddress.getLocalHost(), 80));
	    socket.connect(new InetSocketAddress(InetAddress.getByName(URI), 80));
	    System.out.println("OrangeAPI > Metrics > Socketinput =" + socket.getInputStream());
	    socket.close();
	    
	} catch (IOException e) {

		e.printStackTrace();
	}
	}
	
}
