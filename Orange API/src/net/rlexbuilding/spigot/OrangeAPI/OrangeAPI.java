package net.rlexbuilding.spigot.OrangeAPI;

import java.util.ArrayList;
import java.util.Collection;

import org.bukkit.Bukkit;
import org.bukkit.Chunk;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Egg;
import org.bukkit.entity.EnderPearl;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Pig;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import net.rlexbuilding.spigot.OrangeAPI.Entity.Minecraft_Player;

public class OrangeAPI extends JavaPlugin implements Listener {

	@Override
	public void onEnable() {
		System.out.println("OrangeAPI >>> STARTED");
	}

	public static Minecraft_Player Player() {
		return new Minecraft_Player();
	}

	public static Bossbar Bossbar() {
		return new Bossbar();

	}

	public static TitleAPI Title()

	{
		return new TitleAPI();
	}

	/**
	 * 
	 * @author RLEXBuilding
	 * @return "RLEXBuilding"
	 * 
	 *         Gets the author of the api
	 * 
	 */
	public static String getAPIAuthor() {
	
		return "RLEXBuilding";
	}

}
