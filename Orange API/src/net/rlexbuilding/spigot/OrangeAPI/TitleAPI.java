package net.rlexbuilding.spigot.OrangeAPI;

import org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import net.minecraft.server.v1_10_R1.IChatBaseComponent;
import net.minecraft.server.v1_10_R1.PacketPlayOutChat;
import net.minecraft.server.v1_10_R1.IChatBaseComponent.ChatSerializer;

public class TitleAPI {

	/**
	 * <font color="green" face="Impact">Sends a message in the <b>ACTIONBAR</b></font>
	 */
	public void sendActionbar(Player player, String msg) {
		Player p = player;
		CraftPlayer cp = (CraftPlayer) p;
		IChatBaseComponent icbc = ChatSerializer.a("{\"text\": \"" + msg + "\"}");
		PacketPlayOutChat bar = new PacketPlayOutChat(icbc, (byte) 2);
		cp.getHandle().playerConnection.sendPacket(bar);
	}
	
	/**
	 * <font color="green" face="Impact">Sends a raw message in jsonformat in the <b>ACTIONBAR</b></font>
	 */
	public void sendRawActionbar(Player player, String msg) {
		Player p = player;
		CraftPlayer cp = (CraftPlayer) p;
		IChatBaseComponent icbc = ChatSerializer.a(msg);
		PacketPlayOutChat bar = new PacketPlayOutChat(icbc, (byte) 2);
		cp.getHandle().playerConnection.sendPacket(bar);
	}

}
