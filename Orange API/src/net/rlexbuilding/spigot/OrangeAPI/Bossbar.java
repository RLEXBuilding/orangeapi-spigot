package net.rlexbuilding.spigot.OrangeAPI;

import org.bukkit.Bukkit;
import org.bukkit.boss.BarColor;
import org.bukkit.boss.BarFlag;
import org.bukkit.boss.BarStyle;
import org.bukkit.entity.Player;

public class Bossbar {

	public static void createNewBossbar(String label,BarColor color,BarStyle style,BarFlag flag){
		Bukkit.createBossBar(label, color, style, flag);
	}
	
	
}
