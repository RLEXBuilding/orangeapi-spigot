package net.rlexbuilding.spigot.OrangeAPI.Entity;

import org.bukkit.entity.Arrow;
import org.bukkit.entity.Egg;
import org.bukkit.entity.EnderPearl;
import org.bukkit.entity.EnderSignal;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.SmallFireball;
import org.bukkit.entity.Snowball;

public enum Projectile {

 	
	
	ARROW(Arrow.class),
	EGG(Egg.class),
	SNOWBALL(Snowball.class),
	FIREBALL(Fireball.class),
	SMALL_FIREBALL(SmallFireball.class),
	ENDERPEARL(EnderPearl.class),
	ENDEREYE(EnderSignal.class);

	private Class c;
	private Projectile(Class c){
		this.c = c;
	}
	
	public Class getEntityClass(){
		return this.c;
	}
	
}
