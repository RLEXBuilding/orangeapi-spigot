package net.rlexbuilding.spigot.OrangeAPI.Entity;

import java.io.IOException;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.craftbukkit.v1_10_R1.entity.CraftPlayer;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Egg;
import org.bukkit.entity.EnderPearl;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.mojang.authlib.GameProfile;

import io.netty.buffer.ByteBuf;
import net.minecraft.server.v1_10_R1.Packet;
import net.minecraft.server.v1_10_R1.PacketDataSerializer;
import net.minecraft.server.v1_10_R1.PacketPlayOutCustomPayload;

public class Minecraft_Player {

	public static void sendRawMessage(Player p, String jsonformat) {
		Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "/tellraw " + p.getName() + " " + jsonformat);
	}

	public static String getUUID(Player p) {
		return p.getUniqueId().toString();
	}

	public static String getName(UUID uuid) {

		return Bukkit.getPlayer(uuid).getName();
	}

	public static void setSkin(Player p, String name) {
		GameProfile gp = new GameProfile(p.getUniqueId(), p.getName());

	}
	
	public static void sendPacket(Player p, Packet packet){
	      CraftPlayer cp = (CraftPlayer)p;
	      try {
			packet.a(new PacketDataSerializer(null));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	      PacketPlayOutCustomPayload OPayload = new PacketPlayOutCustomPayload(packet.toString(),null);
	}
	
	
	
	
	

	public static void shootProjectile(Player player,Projectile proj) {
		Class projclass = proj.getEntityClass();
		player.launchProjectile(projclass);
	}


	public static void openEnchantmentTable(Player player) {

		Location loc = new Location(player.getWorld(), 0, 0, 0, 0, 0);
		Block block = (Block) loc;
		block.setType(Material.ENCHANTMENT_TABLE);
		player.openEnchanting(loc, true);
		block.setType(Material.AIR);
		block.setType(Material.BEDROCK);
	}

	public static void sudo(Player player, String command) {
		player.chat("/" + command);
	}

	public static void sendChat(Player player, String message) {
		player.chat(message);
	}

}
